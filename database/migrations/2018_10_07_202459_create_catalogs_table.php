<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_catalogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('catalog_id')->unsigned()->unique();
            $table->string('catalog_name', 255)->nullable(false);
            $table->string('catalog_acronym', 255)->nullable(false);
            $table->string('catalog_shortdescription', 255)->nullable(true);
            $table->text('catalog_description')->nullable(true);
            $table->string('catalog_lastrevisiondate')->nullable(false);
            $table->string('catalog_sourceid', 255)->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_catalogs');
    }
}
