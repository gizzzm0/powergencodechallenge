<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('view.head')
</head>
<body>
@include('view.nav')
<main role="main" class="position-ref full-height container">
    <div class="title m-b-md text-center">
        Data Catalog
    </div>
    <div>
        <div class="row children-center @if(isset($rcatalogs) && count($rcatalogs) > 0) echo 'hidden' @endif"
             id="divLoading">
            <div class="cssload-thecube">
                <div class="cssload-cube cssload-c1"></div>
                <div class="cssload-cube cssload-c2"></div>
                <div class="cssload-cube cssload-c4"></div>
                <div class="cssload-cube cssload-c3"></div>
                <p class="text-center loading-text"><strong>Loading...Please wait</strong></p>
            </div>
        </div>
        <div class="row" id="divContent">
            @if(isset($rcatalogs) && count($rcatalogs) > 0)
                <span id="rCatalogs">{{count($rcatalogs)}}</span>
                @include('view.content', ['catalogs' => $rcatalogs]);
            @endif
        </div>
    </div>
    <div id="descModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTitle"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modalBody">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</main>

<!-- JS -->
@include('view.js')

<script type="text/javascript">
    $(document).ready(function () {
        var $isNonAjax = $('#rCatalogs').text();
        if (typeof($isNonAjax) > 0) {
            $('#divContent').hide();
            getCatalogs(window.location);
        } else {
            getCatalogs('/catalogs');
        }

        $('#divContent').on('click', '.pagination li a', function (e) {
            e.preventDefault();

            $('#divContent').fadeOut();
            $('#divLoading').fadeIn();
            var url = $(this).attr('href');
            getCatalogs(url);
            window.history.pushState("", "", url);
        });

        $('#divContent').on('click', '.viewDesc', function (e) {
            var modalData = $(this).data('info').split('|');
            fillmodalData(modalData);
            $('#descModal').modal('show');
        });

        function getCatalogs(url) {
            $.ajax({
                url: url
            }).done(function (data) {
                $('#divLoading').fadeOut();
                $('#divContent').html(data).fadeIn();
            }).fail(function () {
                alert('Data could not be loaded.');
            });
        }

        function fillmodalData(data) {
            $('#modalTitle').html(data[0]);
            $('#modalBody').html(data[1]);
        }
    })
</script>
</body>
</html>
