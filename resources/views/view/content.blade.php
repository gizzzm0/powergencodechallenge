@if (count($catalogs) > 0)
    @foreach ($catalogs['records'] as $catalog)
        <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <div class="card-header">
                    <h6>{{$catalog['catalog_name']}}</h6>
                </div>
                <div class="card-body">
                    <p class="card-text">{{$catalog['catalog_shortdescription']}}</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <small class="text-muted">
                            <strong>Last Updated:</strong>
                            <br/>
                            {{date('jS F Y', strtotime($catalog['catalog_lastrevisiondate']))}}</small>
                        <div class="btn-group">
                            <button type="button"
                                    class="btn btn-sm btn-outline-secondary viewDesc"
                                    data-info="{{$catalog['catalog_name']}} | {{$catalog['catalog_description']}}">
                                <i class="fa fa-pencil"></i> Full Description
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
<div class="col-md-12 children-center" id="divPagination">
    @if(count($catalogs) > 0)
        {{$catalogs['records']->links()}}
    @endif
</div>