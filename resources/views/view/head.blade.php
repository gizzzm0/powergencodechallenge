<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>{{$pageName}}</title>

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

<!-- Styles -->
<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}"/>
<link rel="stylesheet" href="{{asset('css/custom.css')}}"/>