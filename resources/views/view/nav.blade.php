<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top ">
    <div class="collapse navbar-collapse justify-content-md-center">
        <a class="navbar-brand" href="{{url('/')}}">World Bank Open Data API</a>
    </div>
</nav>