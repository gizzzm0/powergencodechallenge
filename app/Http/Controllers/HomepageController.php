<?php

namespace App\Http\Controllers;

use App\Catalog;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class HomepageController extends Controller
{
    public function __invoke()
    {
        return view('home', ['pageName' => 'Home']);
    }

    public function index(Request $request, $internal = false)
    {
        //Initializing an empty array to populate structured data
        $catalogs = [];

        $current_page = 1;
        $per_page = 500;

        $pageReq = $this->queryAPI($per_page, $current_page);

        //Checking if latest data has been successfully fetched from the API
        if ($pageReq->getStatusCode() == 200) {
            $pageData = json_decode($pageReq->getBody());

            $catalogs['current_page'] = $pageData->page;
            $catalogs['per_page'] = $pageData->per_page;
            $catalogs['total_page'] = $pageData->pages;
            $catalogs['total_records'] = $pageData->total;


            //Checking for (and adding) new data
            if ($pageData->total > $this->catalogCount()) {
                while ($pageData->pages >= $current_page) {
                    $data = json_decode($this->queryAPI($per_page, $current_page)->getBody());
                    //Add metadata
                    $catalogs['records'] = [];

                    //Looping through objects/arrays and creating normalized array
                    if (is_object($data)) {
                        foreach ($data->datacatalog as $res) {
                            $catalog['id'] = $res->id;
                            for ($idx = 0; $idx < sizeof($res->metatype); $idx++) {
                                $meta = $res->metatype[$idx];
                                if ($meta->id == 'name') {
                                    $catalog['name'] = $meta->value;
                                } elseif ($meta->id == 'acronym') {
                                    $catalog['acronym'] = $meta->value;
                                } elseif ($meta->id == 'description') {
                                    $catalog['short_description'] = $this->textTrunc(strip_tags($meta->value)) . ' ...';
                                    $catalog['description'] = $meta->value;
                                } elseif ($meta->id == 'lastrevisiondate') {
                                    $catalog['lastrevisiondate'] = $meta->value;
                                } elseif ($meta->id == 'apisourceid') {
                                    $catalog['apisourceid'] = $meta->value;
                                }
                            }
                            $this->create($catalog);
                        }
                    }
                    $current_page++;
                }

                $catalogs['records'] = $this->allCatalogs();
            } else {
                //Get data from local DB
                $catalogs['records'] = $this->allCatalogs();
            }
        } else {
            //Get data from local DB
            $catalogs['records'] = $this->allCatalogs();
        }

        if ($request->ajax()) {
            return view('view.content', ['catalogs' => $catalogs]);
        } else {
            return view('home', ['pageName' => 'Home', 'rcatalogs' => $catalogs]);
        }
    }

    private function queryAPI($per_page, $current_page)
    {
        //Creating a new API request via Guzzle
        $client = new Client();
        return $client->request('GET', 'http://api.worldbank.org/v2/datacatalog?format=json&per_page=' . $per_page . '&page=' . $current_page);
    }

    private function create($data)
    {
        return Catalog::firstOrCreate([
            'catalog_id' => $data['id'],
            'catalog_name' => $data['name'],
            'catalog_acronym' => $data['acronym'],
            'catalog_shortdescription' => $this->textTrunc(strip_tags($data['description'])) . ' ...',
            'catalog_description' => $data['description'],
            'catalog_lastrevisiondate' => $data['lastrevisiondate'],
            'catalog_sourceid' => $data['apisourceid']
        ]);
    }

    private function allCatalogs()
    {
        return Catalog::paginate(12);
    }

    private function catalogCount()
    {
        return Catalog::count();
    }

    private function textTrunc($text, $length = 180)
    {
        if (strlen($text) > $length) {
            $text = substr($text, 0, strpos($text, ' ', $length));
        }
        return $text;
    }
}
