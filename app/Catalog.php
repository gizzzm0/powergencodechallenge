<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    protected $table = 'tbl_catalogs';

    protected $fillable = [
        'catalog_id',
        'catalog_name',
        'catalog_acronym',
        'catalog_description',
        'catalog_shortdescription',
        'catalog_lastrevisiondate',
        'catalog_sourceid'
    ];

    protected $hidden = ['created_at', 'updated_at'];
}
