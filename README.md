## PowerGen Code Challenge

This is a submission of a code challenge by Powergen as a step in their employee recruitment process.
It is solely built on Web Technologies:   

- Laravel PHP Framework (https://laravel.com).
- Composer (https://getcomposer.org/)
- Javascript
- JQuery (https://jquery.com/)
- Bootstrap 4 Framework (https://getbootstrap.com/)
- MySQL Database

## Local Server Setup

1. Clone project to a folder of your choice
2. Navigate to the project root on your terminal/command prompt
3. Run **composer install** to add dependencies
4. Set up a local HTTP server. For instance, using XAMPP -> https://www.apachefriends.org/
5. Create a new MySQL database
6. Rename the **.env.example** file located at the project root to **.env** and setup your DATABASE CREDENTIALS, APP_KEY(**php artisan key:generate**) and APP_URL. 
   You can also modify all the other variables to correspond to your environment(e.g remove debugging for production code)
7. Either import the included **.sql** file into your database OR run **php artisan migrate** to create new database tables
9. From the project root, run one of the following 
    a). **php -S http://127.0.0.1:8000 -t public** 
        (For Windows users ensure your php executable is accessible as an environmental variable)
        The url and port are customizable to ones of your choosing
    
    or
    b). **php artisan serve**
10. Visit the site on the resultant server
11. YOu can alternatively configure a VirtualHost pointing to your **public** directory

## Live Server Setup(APACHE Shared Hosting)
1. Clone project to a folder of your choice
2. Navigate to the project root on your terminal/command prompt
3. Run **composer install** to add dependencies
4. Rename the **.env.example** file located at the project root to **.env** and setup your DATABASE CREDENTIALS, APP_KEY(**php artisan key:generate**) and APP_URL.
5. Compress, upload and subsequently extract the project to a custom folder on your server root(not **public_html**) for security purposes.
6. Move the contents of the **public** folder into the **public_html** folder (including the .htaccess file)
7. Edit the **index.php** file now in the **public_html** folder and update the paths to correctly reflect the folder structure to your custom folder from No. 5 above. 
8. Import the included **.sql** file to your database
9. Rename the **.env.example** file located at the project root to **.env** and setup your database credentials and APP_KEY.
10. You're all set 

## Author
Gideon Muiru
m.gid3on@gmail.com

## License

The project is licensed under the [MIT license](https://opensource.org/licenses/MIT).